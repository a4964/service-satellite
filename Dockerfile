FROM mcr.microsoft.com/dotnet/aspnet:5.0

WORKDIR /app

COPY ./ServiceSatellite/ServiceSatellite/bin/Release/net5.0/publish/* .

ENTRYPOINT ["dotnet", "/app/ServiceSatellite.dll"]