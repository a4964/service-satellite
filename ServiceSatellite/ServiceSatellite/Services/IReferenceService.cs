﻿using ServiceSatellite.DBContext;
using MessageModel;
using System.Threading.Tasks;

namespace ServiceSatellite.Services
{
    public interface IReferenceService
    {
        /// <summary>
        /// Save the reference into the database
        /// </summary>
        /// <param name="data">JSON data</param>
        /// <param name="context">Database context</param>
        /// <returns>true if the reference was stored correctly or false otherwise</returns>
        public Task<bool> SaveReference(MessageQueue messageQueue, DatabaseContext context);
    }
}
