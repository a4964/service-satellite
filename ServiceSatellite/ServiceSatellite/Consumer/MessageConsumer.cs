using MassTransit;
using MessageModel;
using ServiceSatellite.DBContext;
using ServiceSatellite.Services;
using System;
using System.Threading.Tasks;

namespace ServiceSatellite
{
    internal class MessageConsumer : IConsumer<MessageQueue>
    {
        private DatabaseContext _databaseContext;
        private readonly IReferenceService _referenceService;

        public MessageConsumer(DatabaseContext databaseContext, IReferenceService referenceService)
        {
            _databaseContext = databaseContext;
            _referenceService = referenceService;
        }
        public async Task Consume(ConsumeContext<MessageQueue> context)
        {
            await Console.Out.WriteLineAsync($"Getting data from ServiceApi: IdMessage [{context.Message.IdMessage.ToString()}] Value [{context.Message.Value.ToString()}]");
            await _referenceService.SaveReference(context.Message, _databaseContext);
        }
    }
}
