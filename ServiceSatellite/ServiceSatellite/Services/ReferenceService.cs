﻿using MassTransit;
using MessageModel;
using Microsoft.Extensions.Logging;
using ServiceSatellite.DBContext;
using ServiceSatellite.Models;
using System;
using System.Threading.Tasks;

namespace ServiceSatellite.Services
{
    public class ReferenceService : IReferenceService
    {
        private readonly ILogger<ReferenceService> _logger;
        private readonly IPublishEndpoint _publishEndpoint;

        public ReferenceService(ILogger<ReferenceService> logger, IPublishEndpoint publishEndpoint)
        {
            _logger = logger;
            _publishEndpoint = publishEndpoint;
        }
        public async Task<bool> SaveReference(MessageQueue messageQueue, DatabaseContext context)
        {
            try
            {
                if (messageQueue == null) { throw new Exception("data is null"); }
                Reference reference = new Reference() { MessageId = messageQueue.IdMessage, Value = messageQueue.Value, DateTime = DateTime.Now };
                context.References.Add(reference);
                context.SaveChanges();
                _logger.LogInformation($"Saved reference [{reference.ReferenceId}]");

                ReferenceQueue referenceQueue = new ReferenceQueue() { IdMessage = messageQueue.IdMessage, IdReference = reference.ReferenceId, ReferenceDateTime = reference.DateTime };
                SendReferenceToQueue(referenceQueue);
                return true;
            }
            catch (Exception exp)
            {
                _logger.LogError("Error when saving reference into database", exp);
                return false;
            }
        }

        public async void SendReferenceToQueue(ReferenceQueue referenceQueue)
        {
            await _publishEndpoint.Publish<ReferenceQueue>(referenceQueue);
        }
    }
}
