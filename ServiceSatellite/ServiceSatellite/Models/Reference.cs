﻿using System;

namespace ServiceSatellite.Models
{
    public class Reference
    {
        public int ReferenceId { get; set; }
        public int MessageId { get; set; }
        public int Value { get; set; }
        public DateTime DateTime { get; set; }
    }
}
