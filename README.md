# Service Satellite

Servizio che riceve e spedisce dati tramite RabbitMQ salvandoli su database.

## Build e push immagine Docker

Prima di buildare e pushare l'immagine su registry Docker esterno è necessario buildare e pubblicare l'applicativo tramite Visual Studio o dotnet CLI. Attualmente viene utilizzato la location di pubblicazione di default nella cartella `./ServiceSatellite/ServiceSatellite/bin/Release/net5.0/publish/`.

Successivamente è possibile effettuare il build e push dell'immagine tramite questi comandi:

```
docker build -t andreasosi/microservices-project:service-satellite .

docker push andreasosi/microservices-project:service-satellite

```
