﻿using Microsoft.EntityFrameworkCore;
using ServiceSatellite.Models;

namespace ServiceSatellite.DBContext
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Reference> References { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Map entities to tables  
            modelBuilder.Entity<Reference>().ToTable("References");

            // Configure Primary Keys  
            modelBuilder.Entity<Reference>().HasKey(u => u.ReferenceId).HasName("PK_References");

            // Configure columns  
            modelBuilder.Entity<Reference>().Property(u => u.ReferenceId).HasColumnType("int").UseMySqlIdentityColumn().IsRequired();
            modelBuilder.Entity<Reference>().Property(u => u.MessageId).HasColumnType("int").IsRequired();
            modelBuilder.Entity<Reference>().Property(u => u.Value).HasColumnType("int").IsRequired();
            modelBuilder.Entity<Reference>().Property(u => u.DateTime).HasColumnType("datetime").IsRequired();
        }
    }
}
